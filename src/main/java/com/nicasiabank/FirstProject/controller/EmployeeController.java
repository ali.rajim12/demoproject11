package com.nicasiabank.FirstProject.controller;

import com.nicasiabank.FirstProject.dto.GenericResponse;
import com.nicasiabank.FirstProject.dto.UpdateEmployeeDto;
import com.nicasiabank.FirstProject.exception.ClientException;
import com.nicasiabank.FirstProject.exception.TokenExpireException;
import com.nicasiabank.FirstProject.model.Employee;
import com.nicasiabank.FirstProject.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @author rajim 2021-09-13.
 * @project IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/v1/employees")
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    /**
     * API to fetch all employee list
     *
     * @return Employee List, params employee - id, name , address,
     * contact
     */

    @GetMapping
    public List<Employee> getAllEmployees() {
        return employeeService.getAllEmployeeList();
    }

    @PostMapping
    public GenericResponse createNewEmployee(@RequestBody Employee employee) {
        return employeeService.createNewEmployee(employee);
    }

    /**
     * API to update Employee Detail
     *
     * @param updateEmployeeDto employee detail
     * @param employeeId        employee table Id
     * @return Generic response with success message
     */

    @PutMapping("/{employee_id}")
    public GenericResponse updateEmployee(@RequestBody UpdateEmployeeDto updateEmployeeDto,
                                          @PathVariable(value = "employee_id")
                                                  Long employeeId) {
        return employeeService.updateEmployeeDetail(updateEmployeeDto, employeeId);
    }


    @GetMapping("/object")
    public Object getAllEmployeesObject() {
        return employeeService.getAllEmployeeListObject();
    }

    @GetMapping("/search")
    public Map<Long, String> searchByName(@RequestParam(value = "name")
                                                  String name) {
        if (name.isEmpty()) {
            throw new ClientException("Name is required.");
        }
        return employeeService.searchByName(name);
    }
//
//    // name , name matched
//    Map<String, List<Employee>>

    @GetMapping("/groupBy")
    public Map<String, List<Employee>> getAllGroupBy() {
        return employeeService.findAllEmployeeGroupBy();
    }


//    https://www.section.io/engineering-education/spring-boot-crud-api/
//    https://www.toptal.com/java/spring-boot-rest-api-error-handling#:~:text=ExceptionHandler%20is%20a%20Spring%20annotation,thrown%20within%20this%20controller%20only.
//    https://dzone.com/articles/best-practice-for-exception-handling-in-spring-boo
}
