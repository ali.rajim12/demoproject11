package com.nicasiabank.FirstProject.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author rajim 2021-09-14.
 * @project IntelliJ IDEA
 */
@Setter
@Getter
public class UpdateEmployeeDto {
    private String name;
    private String contact;
}
