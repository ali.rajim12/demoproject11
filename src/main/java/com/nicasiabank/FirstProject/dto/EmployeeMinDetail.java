package com.nicasiabank.FirstProject.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author rajim 2021-09-13.
 * @project IntelliJ IDEA
 */
@Setter
@Getter
public class EmployeeMinDetail {
    private Long employeeId;
    private String name;
}
