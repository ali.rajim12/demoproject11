package com.nicasiabank.FirstProject.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author rajim 2021-09-13.
 * @project IntelliJ IDEA
 */
@Setter
@Getter
@Builder
public class GenericResponse {
    private String message;
    private String status;
}
