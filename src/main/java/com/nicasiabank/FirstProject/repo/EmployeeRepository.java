package com.nicasiabank.FirstProject.repo;

import com.nicasiabank.FirstProject.model.Employee;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author rajim 2021-09-13.
 * @project IntelliJ IDEA
 */
public class EmployeeRepository {

    private static List<Employee> employees = new ArrayList<>();

    public EmployeeRepository() {
//        Employee employee = new Employee();
//        employee.setAddress("Kathmandu");
//        employee.setContact("9849428177");
//        employee.setEmployeeId(1l);
//        employee.setName("Rajim");
//        employees.add(employee);
//
//        Employee employee1 = new Employee();
//        employee1.setAddress("Jhapa");
//        employee1.setContact("9849428177");
//        employee1.setEmployeeId(2l);
//        employee1.setName("Manoj");
//        employees.add(employee1);
    }


    public List<Employee> findAllEmployees() {
        return employees;
    }

    public void submitNewEmployee(Employee employee) {
        employees.add(employee);
    }

    public Optional<Employee> findById(Long employeeId) {
        return employees.stream()
                .filter(e-> e.getEmployeeId() == employeeId)
                .findFirst();
    }

    public void updateEmployee(Employee employee, Long employeeId) {
        OptionalInt indexOpt = IntStream.range(0, employees.size())
                .filter(i -> employeeId == employees.get(i).getEmployeeId())
                .findFirst();

        // Alternate approach to find index of object
        int index = 0;
        for( int i = 0; i<=employees.size(); i++) {
            if(employees.get(i).getEmployeeId() == employeeId) {
                index = i;
                return;
            }
        }
        employees.remove(indexOpt.getAsInt());
        employees.add(index, employee);
    }

    public List<Employee> searchByName(String name) {
        if(name == null) {
            return employees;
        }
        return employees.stream()
                .filter(e-> e.getName().equals(name))
                .collect(Collectors.toList());
    }
}
