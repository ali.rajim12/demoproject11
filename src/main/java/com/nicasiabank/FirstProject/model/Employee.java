package com.nicasiabank.FirstProject.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author rajim 2021-09-13.
 * @project IntelliJ IDEA
 */
@Setter
@Getter
public class Employee {
    private Long employeeId;
    private String name;
    private String contact;
    private String address;
}
