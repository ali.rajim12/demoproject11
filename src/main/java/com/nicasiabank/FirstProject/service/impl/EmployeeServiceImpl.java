package com.nicasiabank.FirstProject.service.impl;

import com.nicasiabank.FirstProject.dto.GenericResponse;
import com.nicasiabank.FirstProject.dto.UpdateEmployeeDto;
import com.nicasiabank.FirstProject.model.Employee;
import com.nicasiabank.FirstProject.repo.EmployeeRepository;
import com.nicasiabank.FirstProject.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author rajim 2021-09-13.
 * @project IntelliJ IDEA
 */
@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    @Override
    public List<Employee> getAllEmployeeList() {
        EmployeeRepository employeeRepository = new EmployeeRepository();
        return employeeRepository.findAllEmployees();
    }

    @Override
    public Object getAllEmployeeListObject() {
        EmployeeRepository employeeRepository = new EmployeeRepository();
        return employeeRepository.findAllEmployees();
    }

    @Override
    public GenericResponse createNewEmployee(Employee employee) {
        EmployeeRepository employeeRepository = new EmployeeRepository();
        employeeRepository.submitNewEmployee(employee);

//        GenericResponse genericResponse = new GenericResponse();
//        genericResponse.setMessage("Mess");

        return GenericResponse.builder()
                .message("Employee saved.")
                .status("OK")
                .build();
    }

    @Override
    public GenericResponse updateEmployeeDetail(UpdateEmployeeDto updateEmployeeDto,
                                                Long employeeId) {
        EmployeeRepository employeeRepository = new EmployeeRepository();
        Optional<Employee> existingEmployee = employeeRepository.findById(employeeId);
        if(!existingEmployee.isPresent()) {
            try {
                throw new Exception("Not Found.");
            } catch (Exception e) {
               return GenericResponse.builder()
                       .message("NOT found ")
                       .build();
            }
        }
        existingEmployee.get().setContact(updateEmployeeDto.getContact());
        existingEmployee.get().setName(updateEmployeeDto.getName());
        employeeRepository.updateEmployee(existingEmployee.get(), employeeId);

        return GenericResponse.builder()
                .status("OK")
                .message("Updated.")
                .build();
    }

    @Override
    public Map<Long, String> searchByName(String name) {
        log.info("Requesting name :: {}", name);
//        if(name == null) {
//            throw new
//        }
        EmployeeRepository employeeRepository = new EmployeeRepository();
        List<Employee> employees = employeeRepository.searchByName(name);
        if(employees.size() == 0) {
            return new HashMap<>();
        }
        Map<Long, String> collect = employees.stream()
                .collect(Collectors.toMap(Employee::getEmployeeId,
                        Employee::getName));
        return collect;
    }

    @Override
    public Map<String, List<Employee>> findAllEmployeeGroupBy() {
        EmployeeRepository employeeRepository = new EmployeeRepository();
        List<Employee> employees = employeeRepository.findAllEmployees();
        
        Map<String, List<Employee>> collect = employees.stream()
                .collect(Collectors.groupingBy(
                        Employee::getName
        ));
        return collect;
    }

}
