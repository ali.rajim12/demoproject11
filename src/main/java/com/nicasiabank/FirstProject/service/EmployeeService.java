package com.nicasiabank.FirstProject.service;

import com.nicasiabank.FirstProject.dto.GenericResponse;
import com.nicasiabank.FirstProject.dto.UpdateEmployeeDto;
import com.nicasiabank.FirstProject.model.Employee;

import java.util.List;
import java.util.Map;

/**
 * @author rajim 2021-09-13.
 * @project IntelliJ IDEA
 */
public interface EmployeeService {
    List<Employee> getAllEmployeeList();

    Object getAllEmployeeListObject();

    GenericResponse createNewEmployee(Employee employee);

    GenericResponse updateEmployeeDetail(UpdateEmployeeDto updateEmployeeDto,
                                         Long employeeId);

    Map<Long, String> searchByName(String name);

    Map<String, List<Employee>> findAllEmployeeGroupBy();
}
